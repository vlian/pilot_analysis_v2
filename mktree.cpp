#include<EdbSegP.h>
#include<EdbPattern.h>
#include<EdbDataSet.h>
int main(int argc, char* argv[]) {
	if (argc < 2) return 1;
	EdbDataProc* dproc = new EdbDataProc(argv[1]);
	dproc->InitVolume(0);
	EdbPVRec* pvr = dproc->GetPVR();
	TFile g(argv[2],"RECREATE");
	for (int i = 0, Npatt = pvr->Npatterns(); i < Npatt; i++) {
		EdbPattern* pat = pvr->GetPattern(i);
		int plate = pat->GetSegment(0)->Plate();
		printf("Plate %d\n",plate);
		TTree *t = new TTree(Form("tree%d",plate),"tree containing BT data");
		Float_t x,y,z,tx,ty;
		Int_t iz,id;
		t->Branch("x",&x,"x/F");
		t->Branch("y",&y,"y/F");
		t->Branch("z",&z,"z/F");
		t->Branch("iz",&iz,"iz/I");
		t->Branch("tx",&tx,"tx/F");
		t->Branch("ty",&ty,"ty/F");
		t->Branch("id",&id,"id/I");
		for (int j = 0, patN = pat->GetN(); j < patN; j++) {
			EdbSegP* s = pat->GetSegment(j);
			x = s->X();
			y = s->Y();
			z = s->Z();
			iz = s->Plate();
			tx = s->TX();
			ty = s->TY();
			id = s->ID();
			t->Fill();
		}
		t->Write();
	}
	delete pvr;
	g.Close();
	TFile h(argv[2]);
	return 0;
}

