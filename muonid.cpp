#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TLeaf.h>
#include<TString.h>
#include<vector>
#include<map>
#include<iterator>
int main(int argc,char* argv[]){
	TFile flink(argv[1]);
	TTree *tlink = (TTree*)flink.Get("tracks");
	TBranch *blink = tlink->FindBranch("s");
	TLeaf *izllink = tlink->FindLeaf("s.eScanID.ePlate");
	TLeaf *idllink = tlink->FindLeaf("eID");
	TLeaf *xllink = tlink->FindLeaf("eX");
	TLeaf *yllink = tlink->FindLeaf("eY");
	TLeaf *zllink = tlink->FindLeaf("eZ");
	TLeaf *txllink = tlink->FindLeaf("eTX");
	TLeaf *tyllink = tlink->FindLeaf("eTY");
	int nentrieslink = tlink->GetEntries();
	printf("%d\n",nentrieslink);
	TFile g(argv[2],"RECREATE");
	TTree *tw[29];
	for(int ipl=0;ipl<29;ipl++){
		tw[ipl] = new TTree(Form("tree%d",ipl+90),Form("BTs in layer %d",ipl+90));
		Float_t xw,yw,zw,txw,tyw;
		Int_t izw,idw;
		tw[ipl]->Branch("x",&xw,"x/F");
		tw[ipl]->Branch("y",&yw,"y/F");
		tw[ipl]->Branch("z",&zw,"z/F");
		tw[ipl]->Branch("iz",&izw,"iz/I");
		tw[ipl]->Branch("tx",&txw,"tx/F");
		tw[ipl]->Branch("ty",&tyw,"ty/F");
		tw[ipl]->Branch("id",&idw,"id/I");
	}
	for(int i=0;i<nentrieslink;i++){
		tlink->GetEntry(i);
		int llen = izllink->GetLen();
		if(llen<5)continue; // At least 5 BTs in track
		int izfirst = izllink->GetValue(0);
		if(izfirst>94)continue; // First BT in track in first 5 layers
		int izlast = izllink->GetValue(llen-1);
		if(izlast<114)continue; // Last BT in track in last 5 layers
		for(int j=0;j<llen;j++){
			idw = idllink->GetValue(j);
			izw = izllink->GetValue(j);
			txw = txllink->GetValue(j);
			if(txw<-0.018||txw>0.002)continue; // BT Tx within 10mrad of -0.008
			tyw = tyllink->GetValue(j);
			if(tyw<0.018||tyw>0.038)continue; // BT Ty within 10mrad of 0.028
			xw = xllink->GetValue(j);
			yw = yllink->GetValue(j);
			zw = zllink->GetValue(j);
			if(i%10000==0)printf("%d / %d %d %d\n",i,nentrieslink,izw,idw);
			tw[izw-90]->Fill();
		}
	}
	for(int ipl=0;ipl<29;ipl++)tw[ipl]->Write();
	g.Close();
	f.Close();
}
