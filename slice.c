void slice(TString filename,TString outname,double xmin,double xmax,double ymin,double ymax){
	TFile f(filename);
	TNtuple *nt = (TNtuple*)f.Get("ntuple");
	TLeaf *l = nt->FindLeaf("x");
	int nentries = nt->GetEntries();
	TFile g(outname,"RECREATE");
	TNtuple *ntwrite = new TNtuple("ntuple","","x:y:z:iz:tx:ty:id:clstruth:etruth");
	for(int i=0;i<nentries;i++){
		nt->GetEntry(i);
		double x = l->GetValue(0);
		if(x<xmin||x>xmax)continue;
		double y = l->GetValue(1);
		if(y<ymin||y>ymax)continue;
		ntwrite->Fill(x,y,l->GetValue(2),l->GetValue(3),l->GetValue(4),l->GetValue(5),l->GetValue(6),l->GetValue(7),l->GetValue(8));
		if(i%100000==0)printf("%d / %d\n",i,nentries);
	}
	ntwrite->Write();
	g.Close();
	f.Close();
}
