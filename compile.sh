g++ mktree.cpp -w `root-config --cflags` -I./Fedra/include -L./Fedra/lib -Wl,-rpath=./Fedra/lib -lEIO -lDataConversion -lEdb -lEbase -lEdr -lScan -lAlignment -lEmath -lEphys -lvt -lDataConversion `root-config --libs` `root-config --evelibs` -o mktree
g++ jsghostrejection.cpp -w `root-config --cflags` -I./Fedra/include -L./Fedra/lib -Wl,-rpath=./Fedra/lib -lEIO -lDataConversion -lEdb -lEbase -lEdr -lScan -lAlignment -lEmath -lEphys -lvt -lDataConversion `root-config --libs` `root-config --evelibs` -o jsghostrejection
g++ jstracking.cpp -w `root-config --cflags` -I./Fedra/include -L./Fedra/lib -Wl,-rpath=./Fedra/lib -lEIO -lDataConversion -lEdb -lEbase -lEdr -lScan -lAlignment -lEmath -lEphys -lvt -lDataConversion `root-config --libs` `root-config --evelibs` -o jstracking
g++ process_sim.cpp -w `root-config --cflags` `root-config --libs` -o process
g++ muonid.cpp -w `root-config --cflags` `root-config --libs` -o muonid
g++ combine.cpp -w `root-config --cflags` `root-config --libs` -o combine
g++ subtract.cpp -w `root-config --cflags` `root-config --libs` -o subtract
