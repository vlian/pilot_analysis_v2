g++ mktree.cpp -w `root-config --cflags` -I./Fedra/include -L./Fedra/lib -Wl,-rpath=./Fedra/lib -lEIO -lDataConversion -lEdb -lEbase -lEdr -lScan -lAlignment -lEmath -lEphys -lvt -lDataConversion `root-config --libs` `root-config --evelibs` -o mktree
g++ combine.cpp -w `root-config --cflags` `root-config --libs` -o combine
g++ subtract.cpp -w `root-config --cflags` `root-config --libs` -o subtract
