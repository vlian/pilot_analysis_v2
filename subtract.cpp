#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TLeaf.h>
#include<TString.h>
#include<vector>
#include<map>
#include<iterator>
int main(int argc,char* argv[]){
	TFile flink(argv[1]);
	TTree *tlink = (TTree*)flink.Get("tracks");
	TBranch *blink = tlink->FindBranch("s");
	TLeaf *izllink = tlink->FindLeaf("s.eScanID.ePlate");
	TLeaf *idllink = tlink->FindLeaf("eID");
	int nentrieslink = tlink->GetEntries();
	printf("%d\n",nentrieslink);
	std::vector<std::vector<int>> linkidtable;
	linkidtable.resize(29);
	for(int i=0;i<nentrieslink;i++){
		tlink->GetEntry(i);
		int llen = izllink->GetLen();
		if(llen<5)continue;
		for(int j=0;j<llen;j++){
			int idlink = idllink->GetValue(j);
			int izlink = izllink->GetValue(j);
			if(i%10000==0)printf("%d / %d %d %d\n",i,nentrieslink,izlink,idlink);
			linkidtable[izlink-90].push_back(idlink);
		}
	}
	TFile f(argv[2]);
	TFile g(argv[3],"RECREATE");
	for(int ipl=0;ipl<29;ipl++){
		TTree *tw = new TTree(Form("tree%d",ipl+90),Form("BTs in layer %d",ipl+90));
		Float_t xw,yw,zw,txw,tyw;
		Int_t izw,idw;
		tw->Branch("x",&xw,"x/F");
		tw->Branch("y",&yw,"y/F");
		tw->Branch("z",&zw,"z/F");
		tw->Branch("iz",&izw,"iz/I");
		tw->Branch("tx",&txw,"tx/F");
		tw->Branch("ty",&tyw,"ty/F");
		tw->Branch("id",&idw,"id/I");
		TTree *t = (TTree*)f.Get(Form("tree%d",ipl+90));
		int nentries = t->GetEntries();
		Float_t x,y,z,tx,ty;
		Int_t iz,id;
		t->SetBranchAddress("x",&x);
		t->SetBranchAddress("y",&y);
		t->SetBranchAddress("z",&z);
		t->SetBranchAddress("iz",&iz);
		t->SetBranchAddress("tx",&tx);
		t->SetBranchAddress("ty",&ty);
		t->SetBranchAddress("id",&id);
		std::map<int,std::vector<Float_t>> btmap;
		for(int i=0;i<nentries;i++){
			t->GetEntry(i);
			btmap.insert(std::pair<int,std::vector<Float_t>>{id,{x,y,z,tx,ty}});
		}
		for(int j=0;j<linkidtable[ipl].size();j++){
			int idsubtract = linkidtable[ipl][j];
			btmap.erase(idsubtract);
		}
		std::map<int,std::vector<Float_t>>::iterator itr;
		int ct = 0;
		for(itr = btmap.begin();itr!=btmap.end();++itr){
			ct++;
			xw = itr->second[0];
			yw = itr->second[1];
			zw = itr->second[2];
			izw = ipl;
			txw = itr->second[3];
			tyw = itr->second[4];
			idw = itr->first;
			tw->Fill();
		}
		printf("layer %d ct = %d\n",ipl+90,ct);
		g.cd();
		tw->Write();
		btmap.clear();
	}
	g.Close();
}
